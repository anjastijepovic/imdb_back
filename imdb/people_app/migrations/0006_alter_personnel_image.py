# Generated by Django 5.0.4 on 2024-04-17 13:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('people_app', '0005_rename_image_url_personnel_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personnel',
            name='image',
            field=models.ImageField(blank=True, upload_to='images/'),
        ),
    ]
