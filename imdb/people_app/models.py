from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.auth.models import User

from watchlist_app.models import WatchList, Movie, Series


class Award(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
    

# actors or directors
class Personnel(models.Model):

    ACTOR = 'Actor'
    ACTRESS = 'Actress'
    DIRECTOR = 'Director'
    ROLE_CHOICES = [
        (ACTOR, 'Actor'),
        (ACTRESS, 'Actress'),
        (DIRECTOR, 'Director'),
    ]
    name = models.CharField(max_length=100)
    birth_date = models.DateField()
    birth_city =  models.CharField(max_length=100)
    birth_country =  models.CharField(max_length=100)
    bio = models.TextField(blank=True)
    image = models.ImageField(blank=True, upload_to='images/')
    awards = models.ManyToManyField(Award, related_name='persons', blank=True)
    movies = models.ManyToManyField(Movie, related_name='persons', blank=True)
    series = models.ManyToManyField(Series, related_name='persons', blank=True)
    role = models.CharField(max_length=10, choices=ROLE_CHOICES)

    def __str__(self):
        return self.name
    

class Review(models.Model):
    review_user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    rating = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    watchlist = models.ForeignKey(WatchList, on_delete=models.CASCADE, related_name="reviews", null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.rating) + " | " + self.watchlist.title
    
    
class Comment(models.Model):
    description = models.CharField(max_length=250, null=True)
    watchlist = models.ForeignKey(WatchList, on_delete=models.CASCADE, related_name="comments", null=True)
    review_user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

