from django.contrib import admin

# Register your models here.

from django.contrib import admin
from .models import Personnel, Review, Award, Comment
# Register your models here.

admin.site.register(Personnel)
admin.site.register(Review)
admin.site.register(Award)
admin.site.register(Comment)

