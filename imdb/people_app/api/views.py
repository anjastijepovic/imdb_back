from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from django.contrib.auth.models import User


from django_filters import rest_framework as filters
from rest_framework.response import Response
from people_app.api  import serializers, permissions
from people_app.models import Personnel, Review, Comment


class PersonnelVS(viewsets.ModelViewSet):
    serializer_class = serializers.PersonnelSerializer
    permission_classes = [permissions.IsAdminOrReadOnly]

    def get_queryset(self):
        pk = self.kwargs['pk']
        personnel = Personnel.objects.filter(movies=pk)
        personnel |= Personnel.objects.filter(series=pk)
        return personnel
    
# class NameStartsWithFilter(filters.FilterSet):
#     name_starts_with = filters.CharFilter(method='filter_by_name_starts_with')

#     class Meta:
#         model = Personnel
#         fields = ['name', 'role']

#     def filter_by_name_starts_with(self, queryset, name, value):
#         print(name, value)
#         return queryset.filter(name__istartswith=value)    
class PersonnelSearch(viewsets.ModelViewSet):
    queryset = Personnel.objects.all()
    serializer_class = serializers.PersonnelSerializer
    permission_classes = [permissions.IsAdminOrReadOnly]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'role']
    # filterset_class = NameStartsWithFilter
    
    def list(self, request):
        role = request.GET.get('role')
        name = request.GET.get('name')
        if name is None:
            name = ''
        queryset = self.queryset.filter(role = role, name__istartswith = name)
        data = self.serializer_class(queryset, many=True)
        return Response(data.data)



class PersonnelDetail(viewsets.ModelViewSet):
    queryset = Personnel.objects.all()
    serializer_class = serializers.PersonnelSerializer
    permission_classes = [permissions.IsAdminOrReadOnly]

    
class ReviewVS(viewsets.ModelViewSet):
    serializer_class = serializers.ReviewSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        pk = self.kwargs['pk']
        return Review.objects.filter(watchlist=pk)
    

class CommentVS(viewsets.ModelViewSet):
    serializer_class = serializers.CommentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
            watchlist = self.kwargs['watchlist']
            return Comment.objects.filter(watchlist=watchlist)
       
    
    def create(self, request,  *args, **kwargs):
        print(request.data)
        serializer = self.get_serializer( data=request.data, partial=True)
        # print(serializer)
        serializer.is_valid(raise_exception=True)
        
        self.perform_create(serializer)
        
        # print(request.data)
        return Response(serializer.data)
    
    def perform_create(self, serializer):
        user_data = self.request.data.get('review_user')
        # print(user_data, 'AAAAAAAAAAAAA')
        instance = serializer.instance

        if user_data is not None:
            review_user = User.objects.filter(id = user_data)
            serializer.save()
            instance = serializer.instance
            instance.review_user = review_user[0]
            serializer.save()

        else:
            serializer.save()
            
  
      
class UserVS(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
    # permission_classes = [IsAuthenticatedOrReadOnly]

 