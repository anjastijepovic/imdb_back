from rest_framework import serializers

from people_app.models import Personnel, Review, Award, Comment
# from watchlist_app.api.serializers import MovieSerializer
from watchlist_app.models import Movie, Series
from django.contrib.auth.models import User
from django.utils import timezone


# extract only date for the created field in comment
class DateOnlyField(serializers.Field):
    def to_representation(self, value):
        if value:
            value = timezone.localtime(value)
            return value.strftime('%Y-%m-%d')
        return None


class ReviewSerializer(serializers.ModelSerializer):
    review_user = serializers.StringRelatedField(read_only=True)
    
    class Meta:
        model = Review
        # exclude =  ('watchlist',)
        fields = "__all__" 


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['username', 'id']


class CommentSerializer(serializers.ModelSerializer):
    review_user = UserSerializer(read_only=True)
    created = DateOnlyField(read_only=True)

    class Meta:
        model = Comment
        fields = "__all__" 

    def get_review_user(self, obj):
       user = obj.review_user.username
       queryset = User.objects.filter(id = user)
       serializer = UserSerializer(instance=queryset)
       return serializer.data


class AwardSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Award
        fields = "__all__" 

# zbog circular import napravila i ja taj serializer
class MovieSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Movie
        fields = "__all__" 


class SeriesSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Series
        fields = "__all__" 


class PersonnelSerializer(serializers.ModelSerializer):

    awards = serializers.SerializerMethodField()
    movies = serializers.SerializerMethodField()
    series = serializers.SerializerMethodField()

    def get_awards(self, awards):
        ids = Personnel.objects.filter(id = awards.id).values_list('awards' , flat=True)
        qs = Award.objects.filter(id__in=ids)
        serializer = AwardSerializer(instance=qs, many=True)
        return serializer.data
    
    def get_movies(self, movies):
        ids = Personnel.objects.filter(id = movies.id).values_list('movies' , flat=True)
        qs = Movie.objects.filter(id__in=ids)
        serializer = MovieSerializer(instance=qs, many=True)
        return serializer.data
    
    def get_series(self, series):
        ids = Personnel.objects.filter(id = series.id).values_list('series' , flat=True)
        qs = Series.objects.filter(id__in=ids)
        serializer = SeriesSerializer(instance=qs, many=True)
        return serializer.data
    
    class Meta:
        model = Personnel
        fields = "__all__" 
        

