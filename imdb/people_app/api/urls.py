from django.urls import path, include, re_path
from rest_framework.routers import DefaultRouter

from people_app.api import views


router = DefaultRouter()
router.register(r'actors-directors-details', views.PersonnelDetail, basename='actors-directors-details') #details about specific actor/director


urlpatterns = [
    path('', include(router.urls)),
    path('<int:pk>/reviews/', views.ReviewVS.as_view({'get': 'list', 'post': 'create'}), name='review-list' ), #access reviews for a particular watchlist with the watchlist id
    path('<int:watchlist>/comments/', views.CommentVS.as_view({'get': 'list', 'post': 'create'}), name='comment-list' ), #access comments for a particular watchlist with the watchlist id
    path('<int:pk>/actors-directors/', views.PersonnelVS.as_view({'get': 'list', 'post': 'create'}), name='actors-directors-list' ), #access actors and directors for a particular watchlist with the watchlist id
    path('actors-directors/', views.PersonnelSearch.as_view({'get': 'list'}), name='actors-directors-search'), #all actors/directors filtering based on name and role
    path('user/', views.UserVS.as_view({'get': 'list'}), name='user-details'), #get all users

]

