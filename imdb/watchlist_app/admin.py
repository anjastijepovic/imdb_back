from django.contrib import admin
from .models import Series, Movie, Episode, Genres
# Register your models here.

admin.site.register(Series)
admin.site.register(Movie)
admin.site.register(Episode)
admin.site.register(Genres)
