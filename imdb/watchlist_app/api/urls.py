from django.urls import path, include

from watchlist_app.api.views import (MoviesAllVS, SeriesAllVS, GenresAllVS,
                                    MoviesPopularVS, SeriesPopularVS, MoviesRecentVS, SeriesRecentVS,
                                   MoviesSearchVS, SeriesSearchVS, MoviesFilterByGenresVS, SeriesFilterByGenresVS,
                                   MovieDetailVS, SeriesDetailVS,
                                   EpisodeDetailVS, EpisodesForSeriesVS)


urlpatterns = [
    
    path('movies/all/', MoviesAllVS.as_view({'get': 'list', 'post': 'create'}), name='movies-all'),
    path('series/all/', SeriesAllVS.as_view({'get': 'list', 'post': 'create'}), name='series-all'),
    path('genres/all/', GenresAllVS.as_view({'get': 'list'}), name='genres-all'),
    
    path('movies/<int:pk>/', MoviesAllVS.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='movie-detail'),
    path('series/<int:pk>/', SeriesAllVS.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='series-detail'), 

    path('movies/popular/', MoviesPopularVS.as_view({'get': 'list'}), name='movies-popular'),
    path('series/popular/', SeriesPopularVS.as_view({'get': 'list'}), name='series-popular'),
    
    path('movies/recent/', MoviesRecentVS.as_view({'get': 'list'}), name='movies-recent'),
    path('series/recent/', SeriesRecentVS.as_view({'get': 'list'}), name='series-recent'),
    
    path('movies/search/', MoviesSearchVS.as_view({'get': 'list'}), name='movies-search'),
    path('series/search/', SeriesSearchVS.as_view({'get': 'list'}), name='series-search'),
    
    path('movies/genres/', MoviesFilterByGenresVS.as_view({'get': 'list'}), name='movies-genres'),
    path('series/genres/', SeriesFilterByGenresVS.as_view({'get': 'list'}), name='series-genres'),
    
    path('episode/<int:pk>/', EpisodeDetailVS.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='episode-detail'),
    path('<int:pk>/episodes/', EpisodesForSeriesVS.as_view({'get': 'list', 'post': 'create'}), name='series-episodes'),

]
