from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import filters
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny, IsAdminUser

from watchlist_app.models import WatchList, Movie, Series, Episode, Genres
from watchlist_app.api.serializers import WatchListSerializer, EpisodeSerializer, SeriesSerializer, MovieSerializer, GenresSerializer

from people_app.api.permissions import IsAdminOrReadOnly


def find_avg_rating(reviews):
    rating_sum = 0
    for review in reviews:
        rating_sum += review['rating']
    return rating_sum / len(reviews)


class MoviesAllVS(viewsets.ModelViewSet):
    serializer_class = MovieSerializer
    queryset = Movie.objects.all()
    permission_classes = [IsAdminOrReadOnly]
    
    def get_queryset(self):
        try:
            pk = self.kwargs['pk']
            return self.queryset.filter(pk=pk)
        except:
           return self.queryset
            
        
    def create(self, request):
        serializer = self.get_serializer( data=request.data, partial=True)
        # print(serializer)
        serializer.is_valid(raise_exception=True)
        
        self.perform_create(serializer)
        
        # print(request.data)
        return Response(serializer.data)
    def perform_create(self, serializer):
        persons_data = self.request.data.get('persons')
        movie_genre_data = self.request.data.get('genres')
        instance = serializer.instance

        if movie_genre_data is not None:
            serializer.save()
            instance = serializer.instance
            ids = [gnr['id'] for gnr in movie_genre_data]
            instance.genres.set(ids)
        else:
            serializer.save()
            
        if persons_data is not None:
            serializer.save()
            ids = [person['id'] for person in persons_data]
            instance.persons.set(ids)
        else:
            serializer.save()
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    def perform_update(self, serializer):
        persons_data = self.request.data.get('persons')
        movie_genre_data = self.request.data.get('genres')
        # print(movie_genre_data)
        instance = serializer.instance

        if movie_genre_data is not None:
            serializer.save()
            instance = serializer.instance
            ids = [gnr['id'] for gnr in movie_genre_data]
            instance.genres.set(ids)
        else:
            serializer.save()
            
        if persons_data is not None:
            serializer.save()
            ids = [person['id'] for person in persons_data]
            instance.persons.set(ids)
        else:
            serializer.save()
        
    # ----------------------------------------------------------------
    
class SeriesAllVS(viewsets.ModelViewSet):
    serializer_class = SeriesSerializer
    queryset = Series.objects.all()    
    permission_classes = [IsAdminOrReadOnly]
    
    def get_queryset(self):
        try:
            pk = self.kwargs['pk']
            return self.queryset.filter(pk=pk)
        except:
           return self.queryset
       
    def create(self, request):
        serializer = self.get_serializer( data=request.data, partial=True)
        # print(serializer)
        serializer.is_valid(raise_exception=True)
        
        self.perform_create(serializer)
        return Response(serializer.data)
    def perform_create(self, serializer):
        persons_data = self.request.data.get('persons')
        series_genre_data = self.request.data.get('genres')
        instance = serializer.instance

        if series_genre_data is not None:
            serializer.save()
            instance = serializer.instance
            ids = [gnr['id'] for gnr in series_genre_data]
            instance.genres.set(ids)
        else:
            serializer.save()
            
        if persons_data is not None:
            serializer.save()
            ids = [person['id'] for person in persons_data]
            instance.persons.set(ids)
        else:
            serializer.save()
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    def perform_update(self, serializer):
        persons_data = self.request.data.get('persons')
        series_genre_data = self.request.data.get('genres')
        # print(movie_genre_data)
        instance = serializer.instance

        if series_genre_data is not None:
            serializer.save()
            instance = serializer.instance
            ids = [gnr['id'] for gnr in series_genre_data]
            instance.genres.set(ids)
        else:
            serializer.save()
            
        if persons_data is not None:
            serializer.save()
            ids = [person['id'] for person in persons_data]
            instance.persons.set(ids)
        else:
            serializer.save()
    
    
class GenresAllVS(viewsets.ModelViewSet):
    serializer_class = GenresSerializer
    queryset = Genres.objects.all()
    permission_classes = [AllowAny]
    
    
class MoviesPopularVS(viewsets.ModelViewSet):
    serializer_class = MovieSerializer
    permission_classes = [AllowAny]
    
    def get_queryset(self):
        result = []

        movies_all = Movie.objects.all()
        for movie in movies_all:
            review_num = movie.reviews.count()
            if review_num > 1:
                result.append(movie)

        result.sort(key=lambda x: -find_avg_rating(x.reviews.values()))
        return result[:5]

 
class SeriesPopularVS(viewsets.ModelViewSet):
    serializer_class = SeriesSerializer
    permission_classes = [AllowAny]
    
    def get_queryset(self):
        result = []

        series_all = Series.objects.all()
        for series in series_all:
            review_num = series.reviews.count()
            if review_num > 1:
                result.append(series)

        result.sort(key=lambda x: -find_avg_rating(x.reviews.values()))
        return result[:5]
    

class MoviesRecentVS(viewsets.ModelViewSet):
    serializer_class = MovieSerializer
    permission_classes = [AllowAny]
    
    def get_queryset(self):
        return Movie.objects.all().order_by('-created')[:5]


class SeriesRecentVS(viewsets.ModelViewSet):
    serializer_class = SeriesSerializer
    permission_classes = [AllowAny]
    
    def get_queryset(self):
        return Series.objects.all().order_by('-created')[:5]


class MoviesSearchVS(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    permission_classes = [AllowAny]
    filter_backends = [filters.SearchFilter]
    search_fields = ['^title']
    
    
class SeriesSearchVS(viewsets.ModelViewSet):
    queryset = Series.objects.all()
    serializer_class = SeriesSerializer
    permission_classes = [AllowAny]
    filter_backends = [filters.SearchFilter]
    search_fields = ['^title']

    
class MoviesFilterByGenresVS(viewsets.ModelViewSet):
    serializer_class = MovieSerializer
    permission_classes = [AllowAny]
    
    def get_queryset(self):
        genre = self.request.query_params.get('genre')
        result = Movie.objects.filter(genres__in=[genre])
        return result
    
    
class SeriesFilterByGenresVS(viewsets.ModelViewSet):
    serializer_class = SeriesSerializer
    permission_classes = [AllowAny]
    
    def get_queryset(self):
        genre = self.request.query_params.get('genre')
        result = Series.objects.filter(genres__in=[genre])
        return result

        
class MovieDetailVS(viewsets.ModelViewSet):
    serializer_class = MovieSerializer
    permission_classes = [IsAdminOrReadOnly]
    # permission_classes = [AllowAny]
    
    
    def get_queryset(self):
        pk = self.kwargs['pk']
        return Movie.objects.filter(pk=pk)

    
class SeriesDetailVS(viewsets.ModelViewSet):
    serializer_class = SeriesSerializer
    permission_classes = [IsAdminOrReadOnly]
    # permission_classes = [AllowAny]
    
    
    def get_queryset(self):
        pk = self.kwargs['pk']
        return Series.objects.filter(pk=pk)


class EpisodeDetailVS(viewsets.ModelViewSet):
    serializer_class = EpisodeSerializer
    permission_classes = [IsAdminOrReadOnly]
    # permission_classes = [AllowAny]
    

    def get_queryset(self):
        pk = self.kwargs['pk']
        return Episode.objects.filter(pk=pk)


class EpisodesForSeriesVS(viewsets.ModelViewSet):
    serializer_class = EpisodeSerializer
    permission_classes = [IsAdminOrReadOnly]
    # permission_classes = [AllowAny]
    

    def get_queryset(self):
        pk = self.kwargs['pk']
        return Episode.objects.filter(series__pk=pk)
