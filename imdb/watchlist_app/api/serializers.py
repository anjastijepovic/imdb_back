from rest_framework import serializers

from watchlist_app.models import WatchList, Episode, Genres, Series, Movie
from people_app.api.serializers import ReviewSerializer, PersonnelSerializer, CommentSerializer


class GenresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genres
        fields = '__all__'
        # exclude = ['id']


class WatchListSerializer(serializers.ModelSerializer):
    genres = GenresSerializer(many=True, read_only=True)
    reviews = ReviewSerializer(many=True, read_only=True)
    persons = PersonnelSerializer(many=True, read_only=True)

    class Meta:
        model = WatchList
        fields = '__all__'


class EpisodeSerializer(serializers.ModelSerializer):
    series = serializers.CharField(source='series.title', read_only=True)

    class Meta:
        model = Episode
        fields = '__all__'


class SeriesSerializer(serializers.ModelSerializer):
    genres = GenresSerializer(many=True, read_only=True)
    reviews = ReviewSerializer(many=True, read_only=True)
    comments = CommentSerializer(many=True, read_only=True)
    persons = PersonnelSerializer(many=True, read_only=True)
    episodes = EpisodeSerializer(many=True, read_only=True)

    class Meta:
        model = Series
        fields = '__all__'


class MovieSerializer(serializers.ModelSerializer):
    genres = GenresSerializer(many=True, read_only=True)
    reviews = ReviewSerializer(many=True, read_only=True)
    comments = CommentSerializer(many=True, read_only=True)
    persons = PersonnelSerializer(many=True, read_only=True)

    class Meta:
        model = Movie
        fields = '__all__'


