from django.db import models

# Create your models here.


class Genres(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Genres"


class WatchList(models.Model):
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=400)
    genres = models.ManyToManyField(Genres, blank=True)
    image = models.ImageField(blank=True, upload_to='images/')
    created = models.DateTimeField(auto_now_add=True)
    

    def __str__(self):
        return self.title

    # class Meta:
    #     abstract = True
    def __str__(self):
        return self.title


class Movie(WatchList):
    def __str__(self):
        return "MOVIE:" + self.title


class Series(WatchList):

    def __str__(self):
        return "SERIES:" + self.title

    class Meta:
        verbose_name_plural = "Series"


class Episode(models.Model):
    series = models.ForeignKey(
        Series, on_delete=models.CASCADE, related_name='episodes')
    episode_number = models.IntegerField()
    episode_title = models.CharField(max_length=50)
    season = models.IntegerField(default=1)

    def __str__(self):
        return "E" + str(self.episode_number) + "S" + str(self.season) + " - " + self.episode_title
