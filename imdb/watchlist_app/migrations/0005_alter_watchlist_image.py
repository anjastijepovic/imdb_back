# Generated by Django 5.0.4 on 2024-04-17 14:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('watchlist_app', '0004_remove_watchlist_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='watchlist',
            name='image',
            field=models.ImageField(blank=True, upload_to='images/'),
        ),
    ]
